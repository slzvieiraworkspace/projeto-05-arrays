
public class MaiorNumero {
	public static void main(String[] args) {

		double[] lista = { -201, -13, -825, -368, -97, -399, -897, -540, -856, -2, -462, -412, -293, -904, -160 };

		if (lista.length == 0) {
			System.out.println("array vazio.");
			return;
		}

		double maximo = lista[0];

		for (double i : lista) {

			if (i > maximo) {
				maximo = i;
			}
		}
		System.out.println(maximo);
	}
}
