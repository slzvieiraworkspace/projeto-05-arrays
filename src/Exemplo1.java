
public class Exemplo1 {

	public static void main(String[] args) {
		
		String[] nomes = new String[4];
		
		nomes[0] = "João";
		nomes[1] = "Manuel";
		nomes[2] = "Maria";
		nomes[3] = "Joaquim";
		
		System.out.println(nomes[0]);
		System.out.println(nomes[2]);
	}
}
