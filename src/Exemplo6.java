
public class Exemplo6 {

	public static void main(String[] args) {
		
		String[] nomes = { "João", "Manuel", "Maria", "Joaquim" };

		for (String n : nomes) {
			System.out.println(n);
		}
	}
}
