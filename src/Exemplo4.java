
public class Exemplo4 {

	public static void main(String[] args) {
		
		String[] nomes = { "João", "Manuel", "Maria", "Joaquim" };
		
		int tamanho = nomes.length;
		
		System.out.println("Seu array tem " + tamanho + " posições");
	}
}
