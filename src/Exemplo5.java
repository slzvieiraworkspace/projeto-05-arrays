
public class Exemplo5 {

	public static void main(String[] args) {
		
		String[] nomes = { "João", "Manuel", "Maria", "Joaquim" };

		for (int i = 0; i < nomes.length; i++) {
			System.out.println(i + ": " + nomes[i]);
		}
	}
}
